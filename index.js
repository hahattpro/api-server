var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');

var port = 3212;

var macCollector = require('./mac-collector');
var imageCollector = require('./image-collector');
var app = express();

app.use('/api/',macCollector);
app.use('/api/',imageCollector);
app.listen(port, ()=>{
    console.log('server up and ready');
});


