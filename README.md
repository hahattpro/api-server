### MAC Collector:
hostname:port/api/mac <br>
Method: POST    <br>
Content-Type: application/json <br>
Parameter: (as application/json) <br>
```
{
    "mac":"AA-BB-CC-DD-EE-FF"
}
```
Return: (as text/plain)<br>
received AA-BB-CC-DD-EE-FF<br>

### Image Collector:<br>
hostname:port/api/photo<br>
Method: POST<br>
Content-Type: multipart/formdata<br>
Parameter:<br>
"image": your file<br>

Return:<br>
"File is uploaded" as text/plain<br>
