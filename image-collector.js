var express = require('express');
var multer = require('multer');

/*
    POST request
    Content-Type: multipart/form-data

    Body:
        "image": your image file
*/

var imageCollector = express.Router();
var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads'); // uploaded image is saved in /uploads folder
  },
  filename: function (req, file, callback) {
    callback(null, file.originalname); // uploaded image keep it original name
  }
});
var upload = multer({ storage : storage}).single('image');

imageCollector.post('/photo',function(req,res){
    upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
        res.end("File is uploaded");
    });
});

module.exports = imageCollector;
