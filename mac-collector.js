var express = require('express');
var bodyParser = require('body-parser');

var store_mac = function(macAddress){
    // TODO: store MAC into database or something
    console.log(macAddress);
}

/*
    POST request
    Header:
        Content-type: application/json
    Body:
    {
       "mac":"AA-BB-CC-DD-EE-FF"
    }

    (with AA-BB-CC-DD-EE-FF is MAC address)

    Response:
        Status: 200
        Content-Type: text/plain
        Body:
            received AA-BB-CC-DD-EE-FF
*/
var macCollector = express.Router();
macCollector.use(bodyParser.json({limit: '50mb'}));
macCollector.post('/mac',(req,res)=>{
    res.setHeader('Content-Type','text/plain');
    var macAddress = req.body.mac;
    store_mac(macAddress);
    res.end('received '+macAddress);
});

module.exports = macCollector;
